#
# Colornauts development environment
#

node default {
    Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

    exec { "apt-update3":
        command => "/usr/bin/apt-get update",
    }
    ->
    package { 'python-software-properties': }
    ->
    apt::ppa { 'ppa:chris-lea/node.js': }
    ->
    exec { "apt-update":
        command => "/usr/bin/apt-get update",
    }
    ->
    class { 'nodejs': }
    package { ['ant', 'openjdk-6-jdk', 'unzip', 'curl']: 
        require => Exec['apt-update']
    }

    # run apt-get update (once) before installing packages
#    Exec["apt-update"] -> Package <| |>
    class { 'apt':
        update_timeout => 0,
    }
    apt::pin { 'sid': priority => 100 }

    class { 'apache':
        default_mods        => false,
        default_confd_files => false,
        default_vhost       => false,
        purge_configs       => true,
        mpm_module          => 'prefork',
    }
    apache::vhost { 'localhost':
        port => '8080',
        docroot => '/vagrant/build/www',
        directoryindex => 'index.html',
        options => ['Indexes','FollowSymLinks','MultiViews'],
    }
    include apache::mod::dir

    package { ['grunt-cli', 'coffee-script', 'grunt-contrib-coffee',
            'grunt-contrib-watch', 'phonegap', 'cordova']:
        ensure => present,
        provider => 'npm',
        require => Class['nodejs'],
    }
    exec { 'npm install':
        path => '/bin:/usr/bin',
        cwd => '/vagrant',
        require => Class['nodejs'],
    }

    archive { 'adt-bundle':
        ensure => present,  
        url => 'http://dl.google.com/android/adt/22.6.2/adt-bundle-linux-x86-20140321.zip',
        target => '/opt',
        timeout => '1200',
        extension => 'zip',
        checksum => false,
        require => [Package['curl'], Package['unzip']],
    }
    file { '/opt/adt-bundle-linux-x86-20140321/sdk':
        mode => 755,
        recurse => true,
        require => Archive['adt-bundle'],
    }

    file { '/home/vagrant/.bash_profile':
        ensure => present,
        content => 'export PATH=${PATH}:/opt/adt-bundle-linux-x86-20140321/sdk/platform-tools:/opt/adt-bundle-linux-x86-20140321/sdk/tools',
    }

    file { '/etc/udev/rules.d/51-android.rules':
        ensure => present,
        source => 'puppet:///modules/udev/51-android.rules',
    }
}

