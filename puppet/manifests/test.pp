#
# Colornauts development environment
#

node default {
    exec { "apt-update3":
        command => "/usr/bin/apt-get update",
    }
    ->
    package { 'python-software-properties': }
    ->
    apt::ppa { 'ppa:chris-lea/node.js': }
    ->
    exec { "apt-update":
        command => "/usr/bin/apt-get update",
    }
    ->
    class { 'nodejs': }

    # run apt-get update (once) before installing packages
#    Exec["apt-update"] -> Package <| |>
    class { 'apt':
        update_timeout => 0,
    }
    apt::pin { 'sid': priority => 100 }

    class { 'apache':
        default_mods        => false,
        default_confd_files => false,
        default_vhost       => false,
        purge_configs       => true,
        mpm_module          => 'prefork',
    }
    apache::vhost { 'localhost':
        port => '80',
        docroot => '/vagrant/build/www',
        directoryindex => 'index.html',
        options => ['Indexes','FollowSymLinks','MultiViews'],
    }
    include apache::mod::dir

    package { ['grunt-cli', 'coffee-script', 'grunt-contrib-coffee',
            'grunt-contrib-watch']:
        ensure => present,
        provider => 'npm',
        require => Class['nodejs'],
    }
    exec { 'npm install':
        path => '/bin:/usr/bin',
        cwd => '/vagrant',
        require => Class['nodejs'],
    }
}

