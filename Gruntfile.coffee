module.exports = (grunt) ->
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-cache-breaker')
  grunt.loadNpmTasks('grunt-wait')


  grunt.registerTask('default', ['clean:html', 'clean:js', 'copy', 'less', 'coffee:compile', 'watch'])
  grunt.registerTask('build', ['clean:html', 'clean:js', 'copy', 'less', 'coffee:compile'])
  grunt.registerTask('build-android', ['build', 'clean:phonegap', 'copy:phonegap'])

  grunt.initConfig

    clean:
      html:
        src: ['build/www/*.html']
      js:
        src: ['build/www/scripts/**/*.js']
      game:
        src: ['build/www/scripts/game/*.js', 'build/www/scripts/game/**/*.js']
      phonegap:
        src: ['build/phonegap/www/*']
      css:
        src: ['build/css/*.css']
  
    copy:
      libjs:
        expand: true
        flatten: true
        filter: 'isFile'
        src: 'src/app/lib/**/*.js'
        dest: 'build/www/scripts/lib/'
      html:
        expand: true
        flatten: true
        src: 'src/app/html/*.html'
        dest: 'build/www/'
      libcss:
        expand: true
        flatten: true
        filter: 'isFile'
        src: 'src/app/lib/**/*.css'
        dest: 'build/www/css/lib'
      png:
        expand: true
        flatten: true
        filter: 'isFile'
        src: 'src/app/gfx/*.png'
        dest: 'build/www/gfx'
               
    coffee:
      options:
        bare: true
      compile:
        expand: true,
        flatten: false,
        cwd: 'src/app/coffee/',
        src: ['*.coffee', '**/*.coffee'],
        dest: 'build/www/scripts/',
        ext: '.js'
    
    less:
      compile:
        files:
          'build/www/css/styles.css': 'src/app/less/styles.less'

    cachebreaker:
      mainjs:
        asset_url: 'scripts/require.js'
        file: 'build/www/index.html'

    phonegap:
      expand: true
      cwd: 'build/www/'
      src: ['**']
      dest: 'build/phonegap/www/'

    watch:
      coffee:
        files: 'src/app/coffee/**/*.coffee'
        tasks: ['clean:game', 'coffee:compile']
      html:
        files: 'src/app/html/*.html'
        tasks: ['clean:html', 'copy:html']

    wait:
      three:
        options:
          delay: 3000


    
