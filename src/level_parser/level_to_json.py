import json
import sys


def level_str_to_json_str(level_str):
    lines = level_str.split("\n")
    array = []
    row_i = -1
    n_columns = lines[0].count('+') - 1
    cur_row = []
    for line in lines:
        # separator lines should start with a "+"
        if line[0] == "+":
            row_i += 1
            if cur_row:  # neglect first
                array.append(cur_row)
            cur_row = [{} for _ in range(n_columns)]
            continue
        # read the values, "|" is the separator
        vals_per_square = line.split("|")[1:-1]
        for i, val_str in enumerate(vals_per_square):
            vals = val_str.split(":")
            if len(vals) == 1:
                if vals[0].strip() != "":
                    cur_row[i][vals[0].strip()] = True
            elif len(vals) == 2:
                cur_row[i][vals[0].strip()] = vals[1].strip()
    return json.dumps(array, indent=2, sort_keys=True)


def read_level_write_json(level_path, json_path):
    with open(level_path, "r") as f:
        level_str = f.read()
    json_str = level_str_to_json_str(level_str)
    with open(json_path, "w") as f:
        f.write(json_str)

if __name__ == "__main__":
    """
    Convert .level files to json format
    (See test.level for an example)
    """
    usage = "Usage:\n\npython level_to_json.py input.level output.json"
    assert len(sys.argv) == 3, usage
    level_path = sys.argv[1]
    json_path = sys.argv[2]
    read_level_write_json(level_path, json_path)
