import unittest
import json
import level_to_json


class Test(unittest.TestCase):

    def test_level_to_json(self):
        self.level_str = open("test.level", "r").read()

        self.level_as_python_structure = \
            [
                [
                    {
                        "c": "b",
                        "r": "g",
                        "star": True
                    },
                    {
                        "c": "r",
                        "r": "rr"
                    }
                ],
                [
                    {
                        "c": "r",
                        "r": "r"
                    },
                    {
                        "c": "r",
                        "end": True
                    }
                ],
                [
                    {
                        "c": "r",
                        "r": "ggr"
                    },
                    {
                        "c": "bb",
                        "r": "r"
                    }
                ],
            ]
        str_is = level_to_json.level_str_to_json_str(self.level_str)
        str_should_be = json.dumps(
            self.level_as_python_structure, indent=2, sort_keys=True)
        assert str_should_be == str_is


if __name__ == '__main__':
    unittest.main()
