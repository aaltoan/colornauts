package fi.ipg

import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest
import spray.http._
import StatusCodes._

class ColornautServiceSpec extends Specification with Specs2RouteTest with ColornautService {
  def actorRefFactory = system
  
  "ColornautService" should {
    "return a list of scores for GET requests to /scores" in {
      Get("/scores") ~> myRoute ~> check {
        responseAs[String] must contain("scores")
      }
    }

    "leave GET requests to other paths unhandled" in {
      Get("/kermit") ~> myRoute ~> check {
        handled must beFalse
      }
    }

    "return a MethodNotAllowed error for PUT requests to the root path" in {
      Put() ~> sealRoute(myRoute) ~> check {
        status === MethodNotAllowed
        responseAs[String] === "HTTP method not allowed, supported methods: GET"
      }
    }
  }
}
