package fi.ipg.scores

case class ScoreTable(scores: List[Score]) {
}

case class Score(playerName: String, count: Int)
