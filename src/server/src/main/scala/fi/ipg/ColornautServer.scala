package fi.ipg

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http

object ColornautServer extends App {

  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("on-spray-can")

  // create and start our service actor
  val service = system.actorOf(Props[ColornautServiceActor], "colornaut-service")

  // start a new HTTP server on port 8081 with our service actor as the handler
  IO(Http) ! Http.Bind(service, interface = "localhost", port = 8081)
}