package fi.ipg

import spray.json._
import DefaultJsonProtocol._
import fi.ipg.scores.Score
import fi.ipg.scores.ScoreTable

object ColornautJsonProtocol extends DefaultJsonProtocol {
  implicit val scoreJson = jsonFormat2(Score)
  implicit val scoreTableJson = jsonFormat1(ScoreTable)
}