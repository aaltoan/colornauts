package fi.ipg

import akka.actor.Actor
import spray.routing._
import spray.http._
import MediaTypes._
import spray.json._
import DefaultJsonProtocol._
import ColornautJsonProtocol._
import fi.ipg.scores.ScoreTable
import fi.ipg.scores.Score

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class ColornautServiceActor extends Actor with ColornautService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)
}

// this trait defines our service behavior independently from the service actor
trait ColornautService extends HttpService {
  val myRoute =
    respondWithMediaType(`application/json`) {
      path("scores") {
        get {
          complete(new ScoreTable(List(new Score("rape", 69))).toJson.prettyPrint)
        }
      }
    } ~
    path("") {
      get {
        respondWithMediaType(`text/html`) { // XML is marshalled to `text/xml` by default, so we simply override here
          complete {
            <html>
              <body>
                <div>omg lol</div>
              </body>
            </html>
          }
        }
      }
    }
}