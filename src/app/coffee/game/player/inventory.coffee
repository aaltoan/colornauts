define ['game/player/item'], (Item) -> 
  
  class Inventory
    constructor: (palette, resources, difficulty) ->
      if resources
        @items = (new Item(palette.getColorByLetter(color), count) for color, count of resources)
      else if Math.random() > difficulty
        @items = (new Item(color, Math.random() * 3 << 0) for color in palette.colors)
      else
        @items = []