define ['game/player/inventory',
        'underscore',
        'easel',
        'tween'], (Inventory, _) ->

  class Player extends createjs.Shape
    constructor: (@grid, game, palette) ->
      @initialize()
      @score = 0
      @inventory = new Inventory(palette, {A: 1, B: 1, C: 1, D: 1})
      @recentMovements = [] #each element: time, dx, dy
      if game.gameVersion == 'Espoo' or game.gameVersion == "Tukholma2"
        createjs.Ticker.addEventListener('tick', @tick)
                
    create: () =>
      @square = @grid.getStartSquare()
      # TODO moveTo?
      @square.visit(@)
      @x = @square.getXPos()
      @y = @square.getYPos()
      @graphics.beginFill('white').drawCircle(@grid.squareWidth / 2, @grid.squareHeight / 2, @grid.squareHeight / 3)

    translateTo: (square) =>
      createjs.Tween.get(@, {override: true})
        .to({x: square.getXPos(), y: square.getYPos()}, 1000, @wiggleOutTween)

    visit: (target) =>
      unless target.isAdjacentTo(@square) or target == @square
        @wiggle(target.xi - @square.xi, target.yi - @square.yi)
        return false
      unless target.visited
        if @canAfford(target)
          target.visit(@)
        else
          @wiggle(target.xi - @square.xi, target.yi - @square.yi)
          return false
      @square = target
      @grid.board.counters.update()
      true

    # Move one square on the grid.
    # params dxi and dyi: differences in square indeces
    # returns: true if moved succesfully, false if aborted and wiggled
    move: (dxi, dyi) =>
      target = @grid.getSquare(@square.xi + dxi, @square.yi + dyi)
      unless target
        @wiggle(dxi, dyi)
        return false
      unless @visit(target)
        @wiggle(dxi, dyi)
        return false
      @moveTo(target)

    moveTo: (target) =>
      dxi = target.xi - @square.xi
      dyi = target.yi - @square.yi
      @recentMovements.push([new Date().getTime(), dxi, dyi])
      @translateTo(target)
      if target.end
        console.log "level ended"
        @nextLevel() if @nextLevel
      return true

    # Navigate to given target square.
    navigateTo: (targetSquare) =>
      steps = @findPathAStar(@square, targetSquare)
      @moveSteps(steps)

    moveSteps: (steps, time=null) =>
      if time is null
        time = 1000/steps.length
      if steps.length > 0
        square = steps.shift()
        return unless @visit(square)
        createjs.Tween.get(@, {override: false})
          .to({x: @square.getXPos(), y: @square.getYPos()}, time, @wiggleOutTween)
          .call(=> @moveSteps(steps, time))



    findPathAStar: (sourceSquare, targetSquare) =>
      startVertex =
        square: sourceSquare
        distFromStart: 0 
        totMinDist: sourceSquare.getDistanceToInIndexUnits(targetSquare)
        parent: null

      endVertex = startVertex
      outSkirt = [startVertex] # vertices on the border of search area
      xiyi_str = sourceSquare.xi+"_"+sourceSquare.yi
      seenSquares = {
        xiyi_str : true 
      }
      while outSkirt.length > 0
        # search ended
        curVertex = outSkirt[0]
        # special treatment if the square is target
        if (curVertex.square is targetSquare) and @canAfford(targetSquare)
            endVertex = curVertex
            break
        # not target, go only if visited in game already:
        else
          if curVertex.square.visited # visited in the play
            # add all neighbours to outSkirt, if not yet visited
            for d in [ [0, -1], [1, 0], [-1, 0], [0, 1] ]
              xi = curVertex.square.xi + d[0]
              yi = curVertex.square.yi + d[1]
              newSquare = @grid.getSquare(xi, yi)
              # newSquare valid?
              if newSquare
                # square already encountered?
                xiyi_str = newSquare.xi+"_"+newSquare.yi
                if seenSquares[xiyi_str]
                  continue
                  # first time seen is the shortest path to it
                else
                  # new vertex
                  xiyi_str = newSquare.xi+"_"+newSquare.yi
                  seenSquares[xiyi_str] = true
                  distFromStart = curVertex.distFromStart + 1
                  newVertex =
                    square: newSquare
                    distFromStart: distFromStart
                    totMinDist: distFromStart + newSquare.getDistanceToInIndexUnits(targetSquare)
                    parent: curVertex
                  outSkirt.push(newVertex)
        # remove the visited node from the list
        outSkirt.shift()
        # sort the outSkirt by totMinDist
        outSkirt = _.sortBy(outSkirt, 'totMinDist')

      # resolve path
      path = []
      while endVertex.parent # ... is not null
        path.push(endVertex.square)
        endVertex = endVertex.parent
      path.reverse()
      return path


    # Displace the player marker without actually moving to a new square.
    # params: dx and dy pixels
    displace: (dx, dy) =>
      createjs.Tween.get(@, {override: true})
        .to({x: @square.getXPos() + dx, y: @square.getYPos() + dy})

    wiggle: (dxi, dyi) =>
      dxi = _.max([-1, _.min([1, dxi])])
      dyi = _.max([-1, _.min([1, dyi])])
      createjs.Tween.get(@, {override: true})
        .to({x: @square.getXPos() + (@grid.squareWidth * dxi / 5), y: @square.getYPos() + (@grid.squareHeight * dyi / 5)}, 50)
        .to({x: @square.getXPos(), y: @square.getYPos()}, 500, @wiggleOutTween)

    wiggleOutTween: (t) ->
      # transforms t to the range [0, 1 + amplitude] for the Tween
      pi2 = Math.PI * 2
      amplitude = 0.25
      nPeriods = 4.0
      if (t == 0 or t == 1) 
        t
      else
        (1-amplitude * Math.pow(2, -6 * t) * Math.cos(t * pi2 * nPeriods))
      
    canAfford: (square) =>
      @itemByColor(square.color).count > 0

    itemByColor: (color) =>
      _.find @inventory.items, (item) -> item.color == color

    tick: =>
      return if @recentMovements.length == 0
      timeNow = new Date().getTime()
      lastMovementTime = @recentMovements[@recentMovements.length - 1][0]
      if timeNow > 500 + lastMovementTime
          @grid.playerStoppedMoving()
