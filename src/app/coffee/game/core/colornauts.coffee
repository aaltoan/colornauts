define ['jquery',
        'game/elements/board',
        'game/elements/palette',
        'game/elements/start_screen',
        'game/core/controls',
        'game/gameplay/levels',
        'easel'], ($, Board, Palette, StartScreen, Controls, Levels) ->

  class Game
    constructor: (args={}) ->
      @difficultyStart = args.difficultyStart || 0.9
      @difficultyIncrement = args.difficultyIncrement || 0.0001
      @difficultyMax = args.difficultyMax || 0.98
      @nColors = args.nColors || 3
      @gameVersion = args.gameVersion || 'Tukholma'

  class Tukholma extends Game
    constructor: (@board, @palette) ->
      super({})

    start: =>
      @board.grid.gameStarted = true
      
      # Enable controls
      #@controls.registerTrailSwipe()
      @controls.registerKeyboard()
      #@controls.registerTapAdjacent()
      @controls.registerTap()
      #controls.registerSwipe()
      @controls.registerAnywhereSwipe()

    nextLevel: =>
      @board.createGrid(@, @palette)
      @board.grid.generateRandom()
      @board.grid.addTopRowContinuous()
      @board.grid.addPlayer()

    width: =>
      8

    height: =>
      6

  class ThinkCompany extends Game
    constructor: (@board, @palette) ->
      super({})
      @levels = new Levels()
      @current = 0

    start: =>
      @board.grid.player.nextLevel = @nextLevel
      @board.grid.gameStarted = true
      @controls.registerKeyboard()
      @controls.registerAnywhereSwipe()

    nextLevel: =>
      unless @levels
        @initLevels()
      @board.createGrid(@, @palette)
      @board.grid.readLevel(@levels.get(@current))
      @board.grid.addPlayer()
      @board.createCounters()
      @current += 1

    destroyLevel: =>
      @grid

    height: =>
      @levels.get(@current).length

    width: =>
      @levels.get(@current)[0].length

  
  class Colornauts
    constructor: (version) ->
      canvasWidth = $('#container').attr('width')
      canvasHeight = $('#container').attr('height')
      gridWidth = canvasWidth * 4 / 5
      gridHeight = canvasHeight

      stage = new createjs.Stage('container')
      createjs.Touch.enable(stage)

      palette = new Palette(5)
      board = new Board(canvasWidth, canvasHeight)
      game = new ThinkCompany(board, palette)
      controls = new Controls(board)
      game.controls = controls
      game.nextLevel()
      stage.addChild(board)
      
      startScreen = new StartScreen(canvasWidth * 4 / 5, canvasHeight, stage, game)
      stage.addChild(startScreen)
      stage.update()

      createjs.Ticker.setFPS(30)
      createjs.Ticker.addEventListener 'tick', (event) ->
        stage.update()
        document.getElementById('score').textContent = board.grid.player.score
        document.getElementById('difficulty').textContent = board.grid.difficulty

      
