define ['jquery', 'jquery.touchSwipe'], ($) ->

  class Controls
    constructor: (@board) ->
      @swiping = false
      @canvas = $('canvas')

    registerKeyboard: =>
      document.onkeydown = (event) =>
        if event.keyCode == 37
          @move(@numericDirection('left'))
        else if event.keyCode == 38
          @move(@numericDirection('up'))
        else if event.keyCode == 39
          @move(@numericDirection('right'))
        else if event.keyCode == 40
          @move(@numericDirection('down'))

    registerSwipe: =>
      @canvas.swipe {
        threshold: 10,
        allowPageScroll: 'none',
        maxTimeThreshold: 150,
        swipeStatus: (event, phase, direction, distance, duration, fingerCount) =>
          console.log phase + " " + direction + " " + distance + " " + duration
          if phase == $.fn.swipe.phases.PHASE_START
            @swiping = true
          else if phase == $.fn.swipe.phases.PHASE_MOVE and direction and @swiping
            nd = @numericDirection(direction)
            # move marker according to the finger
            @board.grid.player.displace(nd.x * distance, nd.y * distance)
            if (nd.x != 0 and distance > @board.grid.squareWidth / 2) or (nd.y != 0 and distance > @board.grid.squareHeight / 2)
              # crossed to another square, ending swipe
              @swiping = false
              @move(@numericDirection(direction))
          else if (phase == $.fn.swipe.phases.PHASE_END or phase == $.fn.swipe.phases.PHASE_CANCEL) and @swiping
            # a swipe ending event received without completing the swipe
            # -> reset swiping and return to center of original square
            @swiping = false
            @move({x: 0, y: 0})
      }

    registerTrailSwipe: =>
      @canvas.swipe {
        threshold: 10,
        allowPageScroll: 'none',
        swipeStatus: (event, phase, direction, distance, duration, fingerCount) =>
          if phase == $.fn.swipe.phases.PHASE_START
            @navigationQueue = []
          else if phase == $.fn.swipe.phases.PHASE_MOVE
            location = {
              x: event.pageX - @canvas.offset().left,
              y: event.pageY - @canvas.offset().top
            }
            square = @board.grid.getSquareByPoint(location)
            if @navigationQueue.length > 1 and square == @navigationQueue[@navigationQueue.length - 2]
              @navigationQueue.pop().unhighlight()
            else if (@navigationQueue.length == 0 and @board.grid.player.square == square) or
                (@navigationQueue.length > 0 and square != @navigationQueue[@navigationQueue.length - 1])
              square.highlight()
              @navigationQueue.push square
          else if phase == $.fn.swipe.phases.PHASE_END
            @board.grid.player.moveSteps @navigationQueue
            square.unhighlight() for square in @navigationQueue
      }

    registerAnywhereSwipe: =>
      @canvas.swipe {
        threshold: 10,
        allowPageScroll: 'none',
        swipeStatus: (event, phase, direction, distance, duration, fingerCount) =>
          location = {
            x: event.pageX - @canvas.offset().left
            y: event.pageY - @canvas.offset().top
          }
          if phase == $.fn.swipe.phases.PHASE_START
            @navigationQueue = []
            @start = location
            @latestSwipeSquare = @board.grid.player.square
          else if phase == $.fn.swipe.phases.PHASE_MOVE
            nextSquare = null
            if Math.abs(location.x - @start.x) > @board.grid.squareWidth * 2 / 3
              nextSquare = @latestSwipeSquare.getAdjacent((if location.x - @start.x > 0 then 1 else -1), 0)
            else if Math.abs(location.y - @start.y) > @board.grid.squareHeight * 2 / 3
              nextSquare = @latestSwipeSquare.getAdjacent(0, (if location.y - @start.y > 0 then 1 else -1))
            if nextSquare and nextSquare == @navigationQueue[@navigationQueue.length - 2]
              @navigationQueue.pop().unhighlight()
            else if nextSquare
              nextSquare.highlight()
              @navigationQueue.push nextSquare
              @start = location
              @latestSwipeSquare = nextSquare
          else if phase == $.fn.swipe.phases.PHASE_END
            @board.grid.player.moveSteps @navigationQueue
            square.unhighlight() for square in @navigationQueue

      }

    registerTap: =>
      @board.grid.setSquareClickListener (square) => @board.grid.player.navigateTo(square)

    registerTapAdjacent: =>
      @board.grid.setSquareClickListener (square) =>
        @board.grid.player.moveTo(square) if square.isAdjacentTo(@board.grid.player.square)

    move: (numDir) =>
      @board.grid.player.move(numDir.x, numDir.y)

    numericDirection: (directionName) =>
      if directionName == 'left'
        {x: -1, y: 0}
      else if directionName == 'up'
        {x: 0, y: -1}
      else if directionName == 'right'
        {x: 1, y: 0}
      else if directionName == 'down'
        {x: 0, y: 1}
      else
        throw 'No such direction ' + directionName
