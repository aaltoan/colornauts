 define ['game/elements/dots'], (Dots) ->
   
   class Counters extends createjs.Container
     constructor: (@items, @xpos, @ypos, @width, @height) ->
       @initialize()
       @update()
 
     update: =>
       @removeAllChildren()
       hSpacing = @height / @items.length
       xpos = @xpos + 0.5 * @width
       for item, i in @items
         ypos = @ypos + (0.5 + i) * hSpacing
         @addChild(new Dots([item], xpos, ypos, hSpacing, @width))