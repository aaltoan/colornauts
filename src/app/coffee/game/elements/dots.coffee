define ['game/gfx/mushroom',
        'underscore', 
        'easel'], (Mushroom, _) -> 
  
  class Dots extends createjs.Container
    constructor: (items, xpos, ypos, hSpacing, width) ->
      @initialize()
      nDots = _.reduce items, ((total, item) -> total + item.count), 0
      degreeSpacing = 2 * Math.PI / nDots
      centerDist = Math.min(hSpacing, width) / 4
      
      this.x = xpos
      this.y = ypos
      
      i = 0
      for item in items
        for j in [0...item.count]
          deg = i * degreeSpacing
          #@graphics.beginFill(item.color.code).beginStroke('white').setStrokeStyle(3)
          #  .drawCircle(xpos + centerDist * Math.cos(deg), ypos + centerDist * Math.sin(deg), centerDist / 2)
            
          this.addChild(new Mushroom(centerDist * Math.cos(deg) - 10, centerDist * Math.sin(deg) - 10, item.color))
          
          i += 1
          

        
