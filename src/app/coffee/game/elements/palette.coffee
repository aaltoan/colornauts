define ['game/elements/color'], (Color) ->  

  class Palette
    constructor: (nColors) ->
      @colors = [
        new Color('Yellow', '#F7941E', 'yellow')
        new Color('Blue', '#1C75BC', 'blue')
        new Color('Green', '#00A651', 'green')
        new Color('Red', '#EF4136', 'red')
      ].slice(0, nColors)

    random: =>
      return @colors[@colors.length * Math.random() << 0]

    getColorByLetter: (letter) =>
      if letter == "A"
        @colors[0]
      else if letter == "B"
        @colors[1]
      else if letter == "C"
        @colors[2]
      else if letter == "D"
        @colors[3]
      else if letter == "E"
        @colors[4]
      else
        throw new Error("No such color defined")
      