define ['easel'], () ->
        
  class Rect extends createjs.Shape
    constructor: (@color, @square) ->
      @initialize()
      @graphics.beginFill(@color).beginStroke('white').setStrokeStyle(1)
          .drawRect(@square.getXPos(), @square.getYPos(), @square.grid.squareWidth, @square.grid.squareHeight)