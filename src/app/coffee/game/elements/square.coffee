define ['game/elements/rect',
        'game/elements/dots',
        'easel',
        'tween'], (Rect, Dots) ->
      
  class Square extends createjs.Container
    constructor: (@color, @xi, @yi, @grid, @inventory) ->
      @initialize()
      @visited = false
      @rect = @addChild(new Rect(@color.code, @))
      coords = @getCenter()
      @dots = @addChild(new Dots(@inventory.items, coords.x, coords.y, @grid.squareWidth, @grid.squareWidth))
      @on('click', => @grid.squareClickListener(@) if @grid.squareClickListener)

    # x pixels from left
    getXPos: =>
      @grid.squareWidth * @xi

    # y pixels from top
    getYPos: =>
      @grid.squareHeight * @yi

    # x and y pixels from left-top corner
    getCenter: =>
      {x: @grid.squareWidth * (@xi + 0.5), y: @grid.squareHeight * (@yi + 0.5)}

    isAdjacentTo: (square) =>
      dxi = Math.abs(square.xi - @xi)
      dyi = Math.abs(square.yi - @yi)
      (dxi == 1 or dyi == 1) and not (dxi == dyi)

    # direction: numeric direction
    getAdjacent: (dxi, dyi) =>
      @grid.getSquare(@xi + dxi, @yi + dyi)

    visit: (player) =>
      player.itemByColor(@color).count -= 1
      for squareItem in @inventory.items
        player.itemByColor(squareItem.color).count += squareItem.count
        player.score += squareItem.count
        squareItem.count = 0
      @removeChild(@dots)
      @visited = true
      @removeChild(@rect)
      @rect = new Rect('grey', @)
      @addChild(@rect)

    highlight: =>
      createjs.Tween.get(@rect).to({alpha: 0.5})

    unhighlight: =>
      createjs.Tween.get(@rect).to({alpha: 1})

    getDistanceToInIndexUnits: (otherSquare) =>
      xiDiff = otherSquare.xi - @xi
      yiDiff = otherSquare.yi - @yi
      return Math.sqrt(xiDiff * xiDiff + yiDiff * yiDiff)

    getNeighbor: (dxi, dyi) =>
      return @grid.getSquare(@xi+dxi, @yi+dyi)
      
