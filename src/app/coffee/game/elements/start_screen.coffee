define ['easel'], ->
  class StartScreen extends createjs.Container
    constructor: (@width, @height, @stage, game) ->
      @initialize()
      bg = new createjs.Shape()
      bg.graphics.beginFill('black')
      bg.graphics.drawRect(0, 0, @width, @height)
      bg.graphics.endFill()
      bg.alpha = 0.5

      text = new createjs.Text('Start', '20px Arial', '#000000')
      text.x = @width / 2 - 25
      text.y = @height / 2 - 10
      
      circle = new createjs.Shape()
      circle.graphics.beginFill('white').drawCircle(@width / 2, @height / 2, 60)
      circle.addEventListener 'click', =>
        game.start()
        # Remove start screen
        @stage.removeChild(@)
      
      @addChild bg
      @addChild circle
      @addChild text
      