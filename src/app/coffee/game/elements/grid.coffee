define ['game/elements/square',
        'game/player/inventory', 
        'game/player/player',
        'underscore',
        'easel'], (Square, Inventory, Player, _) ->  
  
  class Grid extends createjs.Container
    constructor: (@width, @height, @game, @palette, @board) ->
      @initialize()
      @yOffset = 0
      @xOffset = 0
      @nx = @game.width()
      @ny = @game.height()
      @squareWidth = @width / @nx
      @squareHeight = @height / @ny
      @squares = []
      @squareContainer = @addChild(new createjs.Container())
      @difficulty = @game.difficultyStart
      @gameStarted = false

    generateRandom: =>
      for y in [0...@ny]
        @squares.push([])
        for x in [0...@nx]
          color = @palette.random()
          inventory = new Inventory(@palette, false, @difficulty)
          @squares[y].push(@squareContainer.addChild(new Square(color, x, y, @, inventory)))

    readLevel: (levelDefition) =>
      for square in @squares
        @removeChild(square)
      for row, y in levelDefition
        @squares.push([])
        for sd, x in row
          inventory = new Inventory(@palette, sd.resources)
          square = new Square(@palette.getColorByLetter(sd.color), x, y, @, inventory)
          square.end = sd.end
          @squares[y].push(@squareContainer.addChild(square))
          if sd.start
            @startSquare = square

    addPlayer: =>
      @removeChild(@player) if @player
      @player = @addChild(new Player(@, @game, @palette))
      @player.create()

    setSquareClickListener: (listener) =>
      @squareClickListener = listener

    # Get square by e.g. user click location on canvas measured in pixels
    # params: p with properties x and y
    getSquareByPoint: (p) =>
      xi = Math.floor((p.x - @x) / @squareWidth + @xOffset)
      yi = Math.floor((p.y - @y) / @squareHeight + @yOffset)
      @squares[yi] and @squares[yi][xi]

    getSquare: (xi, yi) =>
      @squares[yi + @yOffset] and @squares[yi + @yOffset][xi + @xOffset]

    getStartSquare: () =>
      @startSquare or @squares[@ny / 2][@nx / 2]

    addTopRowContinuous: (outRow = []) =>
      #adds a top row for the continuous version of the game.
      createjs.Tween.get(@).to({y: @y + @squareHeight}, 2000).call(=> @addTopRowContinuous(@squares.pop()))
      @squareContainer.removeChild(square) for square in outRow
      @addRow(-1)

    # FIXME: property of player, move to Player class
    playerStoppedMoving: =>
      dxSum = 0
      dySum = 0
      if @params.gameVersion == 'Espoo'
        dxSum = _.reduce @player.recentMovements, ((total, movement) -> total + movement[1]), 0 
        dySum = _.reduce @player.recentMovements, ((total, movement) -> total + movement[2]), 0 
      if @params.gameVersion == 'Tukholma2'
        dySum = _.reduce @player.recentMovements, ((total, movement) -> total + movement[2]), 0 
        dySum = Math.min(0,dySum)#progress only further
      @addRows(dySum)
      @addColumns(dxSum)
      createjs.Tween.get(@).to({y: @y - dySum * @squareHeight, x: @x - dxSum * @squareWidth}, 200).call(=> @removeRows(dySum)).call(=> @removeColumns(dxSum))
      @player.recentMovements = []

    addRows: (nDy) =>
      dy = nDy / Math.abs(nDy)
      while nDy isnt 0
        @addRow(dy)
        nDy -= dy

    addRow: (dy) =>
      # dy = -1 -> add top row
      # dy = 1  -> add bottom row
      @yOffset += Math.max(0,-dy)
      if dy == -1
        newYi = @getYiLims()[0]+dy
      else #dy == 1
        newYi = @getYiLims()[1]+dy
      newRow = []
      [xiStart, xiEnd] = @getXiLims()
      xiStartToEnd = [xiStart..xiEnd]
      for i in [0...xiStartToEnd.length]
        color = @palette.random()
        inventory = new Inventory(@palette, false, @difficulty)
        newRow.push(@squareContainer.addChild(new Square(color, xiStartToEnd[i], newYi, @, inventory)))
      if dy == -1
        @squares.unshift(newRow)
      else
        @squares.push(newRow)
      @increaseDifficulty() if @gameStarted

    addColumns: (nDx) =>
      dx = nDx / Math.abs(nDx)
      while nDx isnt 0
        @addColumn(dx)
        nDx -= dx

    addColumn: (dx) =>
      # dx = -1 -> add left column
      # dx = 1  -> add right column
      @xOffset += Math.max(0,-dx)
      if dx == -1
        newXi = @getXiLims()[0]+dx
      else #dx == 1
        newXi = @getXiLims()[1]+dx
      [yiStart, yiEnd] = @getYiLims()
      yiStartToEnd = [yiStart..yiEnd]
      for i in [0...yiStartToEnd.length]
        color = @palette.random()
        inventory = new Inventory(@palette, false, @difficulty)
        square = @squareContainer.addChild(new Square(color, newXi, yiStartToEnd[i], @, inventory))
        if dx == -1
          @squares[i].unshift(square)
        else
          @squares[i].push(square)
      @increaseDifficulty()

    removeRows: (nDy) =>
      for i in [0...Math.abs(nDy)]
        if nDy < 0
          squaresToRemove = @squares.pop()
        else
          squaresToRemove = @squares.shift()
          @yOffset -= 1
        @squareContainer.removeChild(square) for square in squaresToRemove

    removeColumns: (nDx) => #nDx is the direction of movement
      for i in [0...Math.abs(nDx)]
        if nDx < 0
          @removeLastColumn()
        else
          @removeFirstColumn()

    removeFirstColumn: =>
      for i in [0...@squares.length]
        @squareContainer.removeChild(@squares[i].shift())
      @xOffset -= 1

    removeLastColumn: =>
      for i in [0...@squares.length]
        @squareContainer.removeChild(@squares[i].pop())


    getXiLims: =>
      xiStart = @squares[0][0].xi
      xiEnd = @squares[0][@squares[0].length - 1].xi
      return [xiStart, xiEnd]

    getYiLims: =>
      yiStart = @squares[0][0].yi
      yiEnd = @squares[@squares.length - 1][0].yi
      return [yiStart, yiEnd]

    increaseDifficulty: =>
      @difficulty += @params.difficultyIncrement unless @difficulty > @params.difficultyMax
