define ['game/elements/grid',
        'game/core/counters',
        'underscore',
        'easel'], (Grid, Counters, _) ->

  class Board extends createjs.Container
    constructor: (@width, @height) ->
      @initialize()

    createGrid: (game, palette) =>
      @removeChild(@grid) if @grid
      @grid = @addChild(new Grid(@width * 4 / 5, @height, game, palette, @))

    createCounters: =>
      @removeChild(@countersBG) if @countersBG
      @removeChild(@counters) if @counters
      #counters background
      @countersBG = new createjs.Shape()
      @addChild(@countersBG)
      @countersBG.graphics.beginFill("gray").drawRect(@grid.width, 0, @width - @grid.width, @height)
      @counters = @addChild(new Counters(@grid.player.inventory.items, @grid.width, 0, @width - @grid.width, @height))
