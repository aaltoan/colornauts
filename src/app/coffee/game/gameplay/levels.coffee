define [], ->
  class Levels
    get: (levelNo) =>
      @levels[levelNo]

    constructor: (palette) ->
      A = "A"
      B = "B"
      C = "C"
      D = "D"
      E = "E"
      @levels = []
      level1 = [
        [
          {
            # rivi 1, sarake 1
            color: B
            resources:
              C: 1
            star: true
          },
          {
            # rivi 1, sarake 2
            color: C
          },
          {
            # rivi 1, sarake 3
            color: A
            end: true
          },
          {
            # rivi 1, sarake 4
            color: A
          },
          {
            # rivi 1, sarake 5
            color: C
            resources: {A: 2}
          }
        ],
        [
          {
            # rivi 2, sarake 1
            color: C
            resources: {B: 1}
          },
          {
            # rivi 2, sarake 2
            color: B
            resources: {A: 1}
          },
          {
            # rivi 2, sarake 3
            color: C
          },
          {
            # rivi 2, sarake 4
            color: C
            resources: {B: 1, C: 1}
          },
          {
            # rivi 2, sarake 5
            color: B
          }
        ],
        [
          {
            # rivi 3, sarake 1
            color: C
          },
          {
            # rivi 3, sarake 2
            color: A
          },
          {
            # rivi 3, sarake 3
            color: B
          },
          {
            # rivi 3, sarake 4
            color: A
            resources: {B: 1}
          },
          {
            # rivi 3, sarake 5
            color: C
            resources: {A: 2, B: 1}
          }
        ],
        [
          {
            # rivi 4, sarake 1
            color: B
          },
          {
            # rivi 4, sarake 2
            color: A
            resources: {C: 2}
          },
          {
            # rivi 4, sarake 3
            color: B
          },
          {
            # rivi 4, sarake 4
            color: A
          },
          {
            # rivi 4, sarake 5
            color: C
          }
        ],
        [
          {
            # rivi 5, sarake 1
            color: C
            resources: {A: 1, C: 1}
          },
          {
            # rivi 5, sarake 2
            color: B
          },
          {
            # rivi 5, sarake 3
            color: A
            resources: {A: 1, B: 1, C: 1}
            start: true
          },
          {
            # rivi 5, sarake 4
            color: C
          },
          {
            # rivi 5, sarake 5
            color: A
            resources: {C: 1}
          }
        ]
      ]
      level2 = [
        [
          {
            # rivi 1, sarake 1
            color: C
            resources:
              C: 1
            star: true
          },
          {
            # rivi 1, sarake 2
            color: A
          },
          {
            # rivi 1, sarake 3
            color: C
            end: true
          },
          {
            # rivi 1, sarake 4
            color: A
          },
          {
            # rivi 1, sarake 5
            color: C
            resources: {A: 2}
          }
        ],
        [
          {
            # rivi 2, sarake 1
            color: C
            resources: {B: 1}
          },
          {
            # rivi 2, sarake 2
            color: B
            resources: {A: 1}
          },
          {
            # rivi 2, sarake 3
            color: C
          },
          {
            # rivi 2, sarake 4
            color: C
            resources: {B: 1, C: 1}
          },
          {
            # rivi 2, sarake 5
            color: B
          }
        ],
        [
          {
            # rivi 3, sarake 1
            color: C
          },
          {
            # rivi 3, sarake 2
            color: A
          },
          {
            # rivi 3, sarake 3
            color: B
          },
          {
            # rivi 3, sarake 4
            color: A
            resources: {B: 1}
          },
          {
            # rivi 3, sarake 5
            color: C
            resources: {A: 2, B: 1}
          }
        ],
        [
          {
            # rivi 4, sarake 1
            color: B
          },
          {
            # rivi 4, sarake 2
            color: A
            resources: {C: 2}
          },
          {
            # rivi 4, sarake 3
            color: B
          },
          {
            # rivi 4, sarake 4
            color: A
          },
          {
            # rivi 4, sarake 5
            color: C
          }
        ],
        [
          {
            # rivi 4, sarake 1
            color: B
          },
          {
            # rivi 4, sarake 2
            color: A
            resources: {C: 2}
          },
          {
            # rivi 4, sarake 3
            color: B
          },
          {
            # rivi 4, sarake 4
            color: A
          },
          {
            # rivi 4, sarake 5
            color: C
          }
        ],
        [
          {
            # rivi 5, sarake 1
            color: C
            resources: {A: 1, C: 1}
            start: true
          },
          {
            # rivi 5, sarake 2
            color: B
          },
          {
            # rivi 5, sarake 3
            color: A
            resources: {A: 1, B: 1, C: 1}
          },
          {
            # rivi 5, sarake 4
            color: C
          },
          {
            # rivi 5, sarake 5
            color: A
            resources: {C: 1}
          }
        ]
      ]
      @levels.push level1
      @levels.push level2