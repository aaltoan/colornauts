define ['easel'], () ->
      
  class Mushroom extends createjs.Container
    constructor: (xpos, ypos, color) ->
      @initialize()
      this.x = xpos
      this.y = ypos
      image = new createjs.Bitmap("gfx/#{color.symbol}_20x20.png")
      this.addChild(image)