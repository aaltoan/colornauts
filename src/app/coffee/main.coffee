require.config {
  shim: {
    'easel': {
      exports: 'createjs'
    },
    'tween': {
      deps: ['easel'],
      exports: 'Tween'
    }
    'jquery.touchSwipe': ['jquery']
  },
  paths: {
    game: 'game',
    easel: 'lib/easeljs',
    tween: 'lib/tweenjs',
    jquery: 'lib/jquery',
    underscore: 'lib/underscore',
    'jquery.touchSwipe': 'lib/jquery.touchSwipe'
  }
}

require ['jquery', 'game/core/colornauts'], ($, Colornauts) ->
  Colornauts('Tukholma')