Getting started
===============

1. Install [VirtualBox](https://www.virtualbox.org).
2. Install [Vagrant](http://www.vagrantup.com).
3. `git clone git@bitbucket.org:aaltoan/colornauts.git`
4. `sh vagrant-up.sh`
5. Browse to [http://localhost:8080](http://localhost:8080).

Test server: [http://188.226.203.176](http://188.226.203.176)
